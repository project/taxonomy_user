
TAXONOMY_USER MODULE

alex_b, Development Seed

INSTALLATION:

Drop taxonomy_user files into your modules directory, go to admin/modules and
turn it on. Go to admin/taxonomy and enable user taxonomy AND free tagging for a vocabulary of your choice.

IMPORTANT:

It is recommended to use a fresh vocabulary for taxonomy_user. On a
taxonomy_user enabled vocabulary, previous node-term relations could be lost if
a node is edited.

If you want to convert consisting term - node relations to taxonomy_user term
relations, use the following SQL script after installation:

  INSERT INTO term_node_user( uid, tid, nid )
  SELECT <uid> , tn.tid, tn.nid
  FROM term_node tn
  JOIN term_data td ON td.tid = tn.tid
  WHERE td.vid = <vid>;
  
  <uid> ... the user id you want to associate the legacy terms to
  <vid> ... the vocabulary that will be user taxonomy enabled

NOTES:

* Only mysql databases are supported so far.
